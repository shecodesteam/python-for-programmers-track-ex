[
{"Title": "Harry Potter", "Author": "J. K. Rolling", "Genre": "Fantasy"},
{"Title": "Anna Karenina", "Author": "Leo Tolstoy", "Genre": "Novel"},
{"Title": "Lolita", "Author": "Vladimir Nabokov", "Genre": "Novel"},
{"Title": "Hamlet", "Author": "William Shakespeare", "Genre": "Tragedy, Drama"},
{"Title": "Crime and Punishment", "Author": "Fyodor Dostoyevsky", "Genre": "Novel, Fiction, Crime Fiction"},
{"Title": "Emma", "Author": "Jane Austen", "Genre": "Novel"},
{"Title": "Mrs. Dalloway", "Author": "Virginia Woolf", "Genre": "Novel"},
{"Title": "To Kill a Mockingbird", "Author": "Harper Lee", "Genre": "Novel, Tragedy"},
{"Title": "The Stranger", "Author": "Albert Camus", "Genre": "Crime Fiction, Novel"},
{"Title": "Rabbit, Run", "Author": "John Updike", "Genre": "Novel"}
]