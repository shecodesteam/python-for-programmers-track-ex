from pprint import pprint
import json
import logging


books_list = []


class Book:
    def __init__(self, title, author, genre):
        self.title = title
        self.author = author
        self.genre = genre
        self.available = True

    def is_book_available(self):
        return self.available

    def order_book(self):
        if self.is_book_available():
            return "%s is not available" % self.title
        self.available = False
        return "you ordered the book %s successfully" % self.title

    def return_book(self):
        self.available = True


def load_books(json_file):
    try:
        with open(json_file) as file:
            json_data = json.load(file)
    except ValueError:
        logging.error("Invalid JSON in " + json_file)
        exit(1)
    except OSError:
        logging.error("Could not open " + json_file)
        exit(1)

    global books_list
    for book in json_data:
        books_list.append(Book(book["Title"], book["Author"], book["Genre"]))


print(books_list)
load_books('./books.js')
pprint(books_list)


