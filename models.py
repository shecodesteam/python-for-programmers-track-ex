import logging
import csv
from homeWork4 import db
from flask_login import UserMixin


class Book(db.Model, UserMixin):
    title = db.Column(db.String(250), unique=True, nullable=False)
    author = db.Column(db.String(120), nullable=False)
    publish_year = db.Column(db.String(4), nullable=False)
    publisher = db.Column(db.String(50), nullable=False)
    url_image = db.Column(db.String(20), nullable=False, default='default.jpg')
    available = True

    def is_book_available(self):
        return self.available

    def order_book(self):
        if not self.is_book_available():
            return "%s is not available" % self.title
        self.available = False
        return "you ordered the book %s successfully" % self.title

    def return_book(self):
        self.available = True


class Library(db.Model, UserMixin):
    name = db.Column(db.String(20), unique=True, nullable=False)
    books = db.relationship('Book', backref='library books', lazy=True)
    users = db.relationship('User', backref='library', lazy=True)

    def add_book(self, title, author, publish_year, publisher, url_image):
        self.books.append(Book(title, author, publish_year, publisher, url_image))

    def add_user(self, name, email, country, address):
        self.users.append(User(name, email, country, address))

    def books_by_publish_year(self, publish_year):
        return [book for book in self.books if book.publish_year == publish_year]

    def find_book_by_title(self, title):
        return [book for book in self.books if book.title == title][0]

    def find_books_by_author(self, author):
        return [book for book in self.books if book.author == author]


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    country = db.Column(db.String(50), unique=False, nullable=False)
    address = db.Column(db.String(200), unique=False, nullable=False)

    def __repr__(self):
        return f"User('{self.name}', '{self.email}')"


def load_books(excel_file):
    books_list = []
    try:
        with open(excel_file) as csv_file:
            csv_reader = csv.reader(csv_file)
            next(csv_reader)
            for row in csv_reader:
                books_list.append(Book(row[1], row[2], row[3], row[4], row[5]))
    except OSError:
        logging.error("Could not open " + excel_file)
        exit(1)
    return books_list


def load_users(excel_file):
    users_list = []
    try:
        with open(excel_file) as csv_file:
            csv_reader = csv.reader(csv_file)
            next(csv_reader)
            for row in csv_reader:
                users_list.append(User(row[1], row[3], row[4], row[5]))
    except OSError:
        logging.error("Could not open " + excel_file)
        exit(1)
    return users_list


books_list = load_books('../books_database.csv')
users_list = load_users('../users_database.csv')
library = Library("The Best Library", books_list, users_list)

