from flask import render_template, url_for, flash, redirect, request, abort
from flask_login import current_user, login_required
from homeWork4 import app, db, bcrypt
from homeWork4.forms import PostUser
from homeWork4.models import User, Book, Library


@app.route('/')
def home():
    return render_template("home.html")


# http://localhost:8000/users
@app.route("/users", methods=['GET'])
def get_users():
    users = User.query.all()
    return render_template("Users.html",
                           title="Users",
                           users=users)


@app.route("/users/new", methods=['GET', 'POST'])
def new_post():
    form = PostUser()
    if form.validate_on_submit():
        user = User(name=form.name.data, email=form.email.data, country=form.country.data, address=form.address.data)
        flash('Your user has been created!', 'success')
        return redirect(url_for('home'))
    return render_template('create_user.html', title='New User',
                           form=form, legend='New User')


@app.route("/users/<int:user_id>", methods=['GET'])
def show_user(user_id):
    user = User.query.get_or_404(user_id)
    return render_template('User.html', name=user.name, email=user.email, country=user.country, address=user.address)


@app.route("/users/<int:user_id>/update", methods=['GET', 'PUT'])
def update_post(user_id):
    user = User.query.get_or_404(user_id)
    if user.name != current_user:
        abort(403)
    form = PostUser()
    if form.validate_on_submit():
        user.name = form.name.data
        user.email = form.email.data
        db.session.commit()
        flash('User has been updated!', 'success')
        return redirect(url_for('user', user_id=user.id))
    elif request.method == 'GET':
        form.name.data = user.name
        form.email.data = user.email
    return render_template('create_user.html', title='Update User',
                           form=form, legend='Update User')


@app.route("/users/<int:user_id>/delete", methods=['POST'])
def delete_user(user_id):
    user = User.query.get_or_404(user_id)
    if user.name != current_user:
        abort(403)
    db.session.delete(user)
    db.session.commit()
    flash('User has been deleted!', 'success')
    return redirect(url_for('home'))


if __name__ == '__main__':
    app.run(debug=True)
